package sales.service.model;

import javax.persistence.*;
import java.util.Date;

/**
 * @author Yerko Manzel
 */
@Entity
@Table(name = "sale_table")
public class Sale {

    @Id
    @Column(name = "saleid", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(name = "numberSale", nullable = false)
    private Long numberSale;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "createdDate", nullable = false, updatable = false)
    private Date createdDate;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "saleemployeeid", referencedColumnName =
            "employeeid", nullable = false)
    private Employee employee;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "saleclientid", referencedColumnName =
            "clientid", nullable = false)
    private Client client;

    @OneToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "saledetailid", referencedColumnName =
            "detailid", nullable = false)
    private Detail detail;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getNumberSale() {
        return numberSale;
    }

    public void setNumberSale(Long numberSale) {
        this.numberSale = numberSale;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Detail getDetail() {
        return detail;
    }

    public void setDetail(Detail detail) {
        this.detail = detail;
    }
}
