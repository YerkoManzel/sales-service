package sales.service.model;

import javax.persistence.*;

/**
 * @author Yerko Manzel
 */
@Entity
@Table(name = "employee_table")
@PrimaryKeyJoinColumns({
        @PrimaryKeyJoinColumn(name = "employeeid",
                referencedColumnName = "personid")
})
public class Employee extends Person {

    @Column(name = "position", nullable = false)
    private String position;

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }
}
