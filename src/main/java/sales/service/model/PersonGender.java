package sales.service.model;

/**
 * @author Yerko Manzel
 */
public enum PersonGender {
    MALE,
    FEMALE
}
