package sales.service.model;

/**
 * @author Yerko Manzel
 */
public enum PersonState {
    ACTIVATED,
    DEACTIVATED
}
